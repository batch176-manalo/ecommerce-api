//Dependencies and Modules
	const mongoose = require('mongoose');
	const Store = require('../models/Store');
	const auth = require('../auth.js');
	const fs = require("fs");
	const Product = require('../models/Product');
	
//1. Create Store (Admin Only)

	module.exports.registerStore = async (req) => {
		let storeName = req.body.storeName;
		let category = req.body.category;
		let address = req.body.address;
				
		let newStore = new Store({
			storeName: storeName,
			category: category,
			address: address,
			image: {
		      data: fs.readFileSync("uploads/" + req.file.filename),
		      contentType: "image/png, image/jpeg",
		    },
		});

		let storeExists = await Store.findOne({ storeName: storeName }).then((result, err) => {
			
				if(result) {
					return true;
				} else {
					return false;
				}
		})

		if (storeExists) {
			
			return false;

		} else {

			return newStore.save().then((store, err) => {

				if (store) {
					return store;
				} else {
					return {message: 'Failed to Register the Store'};
				} 
			})

		}	
	} 


//2. Retrieve All Stores
	module.exports.getAllStores = () => {
		return Store.find().then(result => {

			if (result.length === 0) {
				return "There's no registered Store.";
			} else {
				return result;
			}
			
		}).catch(error => error)
	}

//3. Retrieve Store's Products
	module.exports.getStoreProducts = (storeId) => {
		
		return Product.find({storeId: storeId}).then(products =>  {

			if (products.length === 0) {
				return false;
			} else {
				return products;
			}
		})

	}

//4. Retrieve Store's Details
	module.exports.getStoreDetails = (storeId) => {
		return Store.findById(storeId);
	}

//5. Update Store Information (Admin only)
	module.exports.updateStore = (storeId, req) => {

		let updatedStore = {
				storeName: req.body.storeName,
				category: req.body.category,
				address: req.body.address,
			};

		return Store.findByIdAndUpdate(storeId, updatedStore).then((store, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
			}).catch(error => error)
	}


//6. Update Store Information with Image (Admin only)

	module.exports.updateStoreImage = (storeId, req) => {

		let updatedStore = {
				storeName: req.body.storeName,
				category: req.body.category,
				address: req.body.address,
				image: {
			      data: fs.readFileSync("uploads/" + req.file.filename),
			      contentType: "image/png, image/jpeg",
			    }
			};

		return Store.findByIdAndUpdate(storeId, updatedStore).then((store, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}


//7. Deactivate Store
	module.exports.deactivateStore = (storeId) => {
		let disableStatus = {
			isActive: false
		}
		
		return Store.findByIdAndUpdate(storeId, disableStatus).then((store, error) => {

			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}

//8. Activate Store
	module.exports.activateStore = (storeId) => {
		let activateStatus = {
			isActive: true
		}
		
		return Store.findByIdAndUpdate(storeId, activateStatus).then((store, error) => {

			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}


//9. Delete Store

	module.exports.deleteStore = async (req) => {

		const deleteStoreProducts = await Product.deleteMany({"storeId": req.body.storeId});

		const deleteStore = await Store.deleteOne({"_id": req.body.storeId});

	}