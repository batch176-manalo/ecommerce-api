//Dependencies and Modules
	const mongoose = require('mongoose');
	const Order = require('../models/Order');
	const User = require('../models/User'); 
	const Product = require('../models/Product');
	const Store = require('../models/Store');
	const auth = require('../auth.js');
	
//1. Create Order (Non-admin)

	module.exports.addOrder = async (req, res) => {
		
		let isProductActive = await Product.findById(req.body.productId).then(result => {

			if (result.isActive) {
				return true
			} else {
				return false
			}

		});
		
		let userExists = await Order.findOne({userId: req.user.id}).then(result => {

			if (result) {
				return true
			} else {
				return false
			}
		});


		if (isProductActive) {

			let storeName = await Store.findById(req.body.storeId).then(result => result.storeName);
			let itemPrice = await Product.findById(req.body.productId).then(result => result.price);	
			let itemsAmount = itemPrice*req.body.quantity;
			let itemName = await Product.findById(req.body.productId).then(result => result.name)
			let itemImage = await Product.findById(req.body.productId).then(result => result.image)

			if (userExists) {

				let storeExists = await Order.findOne({"userId": req.user.id, "storeId": req.body.storeId}).then(result => {
					
					if (result) {
						return true;
					} else {
						return false;
					}

				});
				
				if (storeExists) {

					let productExists = await Order.findOne(
							{	"userId" : req.user.id,
						   	 	"storeId": req.body.storeId,
						   	 	"products": { $elemMatch: { productId: req.body.productId } } 
						   	}
						)

					if (productExists) {

						let addToProduct =  await Order.findOneAndUpdate(
						   { "userId" : req.user.id,
						   	 "storeId": req.body.storeId,
						   	 "products": { $elemMatch: { productId: req.body.productId  } } 
						   },
						   { $inc: { "totalAmount": itemsAmount, "products.$[elem].quantity" : req.body.quantity, "products.$[elem].subtotal" : itemsAmount } },
						   { arrayFilters: [ { "elem.productId": req.body.productId } ] }
						)
						
						res.send(addToProduct)
							
					} else {

						let additionalProduct = {
									
							productId: req.body.productId,
							quantity: req.body.quantity,
							subtotal: itemsAmount,
							name: itemName,
							price: itemPrice,
							image: itemImage
							
						}

						let updatedFields = await Order.findOne({"userId": req.user.id, "storeId": req.body.storeId}).then(order => {

							order.products.push(additionalProduct);
							order.totalAmount += itemsAmount;
							return order.save().then(order => order).catch(err => err.message);

						})
				
						res.send(updatedFields)
					}

				} else {

					let newOrder = new Order({

						userId: req.user.id,
						storeId: req.body.storeId,
						storeName: storeName,
						products: [{
							productId: req.body.productId,
							quantity: req.body.quantity,
							subtotal: itemsAmount,
							name: itemName,
							price: itemPrice,
							image: itemImage
						}],
						totalAmount: itemsAmount
						
					})

					return newOrder.save().then((order, error) => {
						if (order) {
							res.send(order);
						} else {
							res.send(message = 'Failed to Create New Order');
						}
					})	
				}

			} else {

				let newOrder = new Order({
					userId: req.user.id,
					storeId: req.body.storeId,
					storeName: storeName,
					products: [{
						productId: req.body.productId,
						quantity: req.body.quantity,
						subtotal: itemsAmount,
						name: itemName,
						price: itemPrice,
						image: itemImage
					}],
					totalAmount: itemsAmount
			
				})

				return newOrder.save().then((order, error) => {
					if (order) {
						res.send(order);
					} else {
						res.send(message = 'Failed to Create New Order');
					}
				})	
			}

		} else {
			res.send(message = "Sorry. The Item is currently Unavailable.")
		}		
	}


//2. Retrieve all orders (Admin Only)
	module.exports.getAllOrders = () => {
		return Order.find()
	}

//3. Edit Order

	module.exports.editOrder = async (req) => {

		let orderData = await Order.findOne({"_id": req.body.orderId});
		let productData = orderData.products.find(({productId}) => productId === req.body.productId);
		
		let itemPrice = productData.price;	
		let itemsAmount = itemPrice*req.body.quantity;
		let previousSubtotal = await productData.subtotal;
		let totalAmount = orderData.totalAmount + itemsAmount - previousSubtotal;

		let newOrder =  await Order.findOneAndUpdate(
		   { "_id" : req.body.orderId,
		   	 "products": { $elemMatch: { productId: req.body.productId } } 
		   },
		   { $set: { "totalAmount": totalAmount, "products.$[elem].quantity" : req.body.quantity, "products.$[elem].subtotal" : itemsAmount } },
		   { arrayFilters: [ { "elem.productId": req.body.productId } ] }
		)
		
		return newOrder

	}

//4. Remove Order

	module.exports.removeOrder = async (req) => {

		 await Order.deleteOne({"_id": req.body.orderId});
		 return true;
	}

//5. Remove Order Item 

	module.exports.removeItem = async (req) => {

		const orderData = await Order.findOne({"_id": req.body.orderId});
		const productId = req.body.productId;
		const prods = await orderData.products;
		const prod = await prods.find(x => x.productId == req.body.productId);
		const sub = prod.subtotal;
		const newTotal = orderData.totalAmount - sub;


		await Order.findOneAndUpdate({ "_id" : req.body.orderId}, { "totalAmount": newTotal } );

		await Order.findOneAndUpdate(
		   { "_id" : req.body.orderId,
		   	 "products": { $elemMatch: { productId: req.body.productId } } 
		   },
		   { $pull: { "products": { productId: req.body.productId } } },
		   { arrayFilters: [ { "elem.productId": req.body.productId } ] }
		);

		let newData = await Order.findOne({"_id": req.body.orderId});
		let totalCheck = newData.totalAmount;

		if (totalCheck <= 0) {
			await Order.deleteOne({"_id": req.body.orderId});
			return {message: "Order deleted"};
		} else {
			return {message: "Order updated"};
		}
	}

	
