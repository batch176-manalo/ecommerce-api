//Dependencies and Modules
	const mongoose = require('mongoose');
	const History = require('../models/History');
	const Order = require('../models/Order');


//1. Move Order to History Collection

module.exports.moveToHistory = async (req) => {


	const order = await Order.findOne({"_id": req.body.orderId}).exec();
	
	let history = new History({
		userId: req.user.id,
		storeId: order.storeId,
		storeName: order.storeName,
		products: order.products,
		totalAmount: order.totalAmount		
	});	
	
	await history.save();
	await Order.deleteOne({"_id": req.body.orderId});
	return history;

}

//2. View History

module.exports.viewHistory = (req) => {	
	
		return History.find({"userId": req.user.id});

}