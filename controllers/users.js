//Dependencies and Modules
	const mongoose = require('mongoose');
	const User = require('../models/User');
	const Order = require('../models/Order');
	const bcrypt = require('bcrypt'); 
	const dotenv = require('dotenv');
	const auth = require('../auth.js');

//Environment Variables Setup
	dotenv.config();
	const salt = Number(process.env.SALT);

//Destructure auth
	const {createAccessToken} = auth;

//1. User Registration
	module.exports.register = async (userData) => {
		let email = userData.email;
		let passW = userData.password;
		let fName = userData.firstName;
		let lName = userData.lastName
				
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, salt)
		});

		let emailExists = await User.findOne({ email: email }).then((result, err) => {
			
				if(result) {
					return true;
				} else {
					return false;
				}
		})
		
		if (emailExists) {
			
			return false;

		} else {

			return newUser.save().then((user, err) => {

				if (user) {
					return user;
				} else {
					return {message: 'Failed to Register Account'};
				} 
			})

		}	
	} 

//2. User Authentication
	module.exports.loginUser = (data) => {
		return User.findOne({email: data.email}).then(result => {

			if(result === null) {
				return false;
			} else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);

				if(isPasswordCorrect) {
					return {accessToken: createAccessToken(
						result.toObject())};
				} else {
					return false;
				}
			}
			
		})
	}

//3. Set user as Admin (Admin only)
	module.exports.setAsAdmin = (userId) => {
		let setAdmin = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(userId, setAdmin).then((user, error) => {
			if (user) {
				return 'User is set as Admin successfully.';
			} else {
				return 'Something went wrong';
			}
		})
	}

//4. Retrieve Authenticated User's Order
	module.exports.getUserOrders = (req, res) => {
		const userId = mongoose.Types.ObjectId(req.user.id);
		
		Order.find({userId: userId}).then(result => {
			
			if (result.length === 0) {
				res.send("You have no order yet.")
			} else {
				res.send(result)
			}
		})
	}

//5. Retrieve All Users (Admin Only)
	module.exports.getAllUsers = () => {
		return User.find().then(result => {
			return result;
		}).catch(error => error)
	}

//6. Retrieve User's Details
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			result.password = '';
			return result;
		})
	}



