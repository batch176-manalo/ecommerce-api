//Dependencies and Modules
	const mongoose = require('mongoose');
	const multer = require('multer');
	const Product = require('../models/Product');
	const Store = require('../models/Store');
	const fs = require("fs");

//1. Create Product (Admin only)
	module.exports.addProduct = (req) => {

		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			image: {
		      data: fs.readFileSync("uploads/" + req.file.filename),
		      contentType: "image/png, image/jpeg",
		    },
		    storeId: req.body.storeId
		});

		return newProduct.save().then((product, error) => {
			
			if (error) {
				return false;
			} else {
				return product;
			}

		}).catch(error => error)

	}

//2. Retrieve all Active Products
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {

			if (result.length === 0) {
				return "There's no Active product.";
			} else {
				return result;
			}
			
		}).catch(error => error)
	}

//3. Retrieve all Inactive Products (Admin Only) - Additional Feature
	module.exports.getAllinActive = () => {
		return Product.find({isActive: false}).then(result => {

			if (result.length === 0) {
				return "There's no Inactive product.";
			} else {
				return result;
			}
			
		}).catch(error => error)
	}

//4. Retrieve Single Product
	module.exports.getProduct = (reqParams) => {
		return Product.findById(reqParams).then(result => {
			return result
		}).catch(error => error)
	}

//5. Update Product Information (Admin only)
	module.exports.updateProduct = (productId, data) => {
		let updatedProduct = {
			name: data.name,
			description: data.description,
			price: data.price
		}

		return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if (error) {
				return 'Something went wrong.';
			} else {
				return 'Item successfully updated.';
			}
		}).catch(error => error)
	}

//6. Update Product Information with Image (Admin only)
	module.exports.updateProductImage = (productId, req) => {
		let updatedProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			image: {
		      data: fs.readFileSync("uploads/" + req.file.filename),
		      contentType: "image/png, image/jpeg",
		    }
		}

		return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if (error) {
				return 'Something went wrong.';
			} else {
				return 'Item successfully updated.';
			}
		}).catch(error => error)
	}


//7. Archive Product (Admin only)
	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
			isActive: false
		}

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}

//8. Reactivate Product (Admin only) - Additional Feature
	module.exports.reactivateProduct = (productId) => {
		let updateActiveField = {
			isActive: true
		}

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}


//9. Delete Product (Admin only)

	module.exports.deleteProduct = async (req) => {

		const remove = await Product.deleteOne({"_id": req.body.productId})

	}