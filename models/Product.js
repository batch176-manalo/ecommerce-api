//Modules and Dependencies
	const mongoose = require('mongoose');

//Schema 
	const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Name is Required']
		},
		description: {
			type: String,
			required: [true, 'Description is Required']
		},
		price: {
			type: Number,
			required: [true, 'Price is Required'] 
		},
		image: {
			data:Buffer,
			contentType: String			
		},
		isActive: {
			type: Boolean,
			default: true,
		}, 
		createdOn: {
			type: Date,
			default: new Date()
		},
		storeId: {
			type: mongoose.ObjectId,
			required: [true, 'storeId is Required']
		},
	});

//Model
	module.exports = mongoose.model('Product', productSchema);
	