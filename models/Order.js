//Modules and Dependencies
	const mongoose = require('mongoose');
	
	
//Schema 
	const orderSchema = new mongoose.Schema({
		userId: {
			type: mongoose.ObjectId,
			required: [true, 'userId is Required']
		},
		storeId: {
			type: String,
			required: [true, 'userId is Required']
		},
		storeName: {
			type: String,
			default: ''
		},
		products: [{
			productId: {
				type: String,
				required: [true, 'productId is Required']
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is Required']
			},
			subtotal: {
				type: Number,
				default: 0
			},
			name: {
				type: String,
				deafult: ''
			},
			price: {
				type: Number,
				deafult: 0
			},
			image: {
				data:Buffer,
				contentType: String			
			}
		}],
		totalAmount: {
			type: Number,
			default: 0
		}		
	});

//Model
	module.exports = mongoose.model('Order', orderSchema);


