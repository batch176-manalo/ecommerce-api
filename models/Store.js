//Modules and Dependencies
	const mongoose = require('mongoose');

//Schema 
	const storeSchema = new mongoose.Schema({
		storeName: {
			type: String,
			required: [true, `Store's name is Required`]
		},
		category: {
			type: String,
			required: [true, 'Category is required.']
		},
		address: {
			type: String,
			required: [true, 'Location is Required']
		},
		image: {
			data:Buffer,
			contentType: String			
		},
		isActive: {
			type: Boolean,
			default: true,
		}, 	
	});

//Model
	module.exports = mongoose.model('Store', storeSchema);

