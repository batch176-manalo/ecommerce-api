//Modules and Dependencies
	const mongoose = require('mongoose');
	
	
//Schema 
	const historySchema = new mongoose.Schema({
		userId: {
			type: String,
			required: [true, 'userId is Required']
		},
		storeId: {
			type: String,
			required: [true, 'userId is Required']
		},
		storeName: {
			type: String,
			required: [true, 'Store Name is Required']
		},
		products: [{
			productId: {
				type: String,
				required: [true, 'productId is Required']
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is Required']
			},
			subtotal: {
				type: Number,
				required: [true, 'Subtotal is Required']
			},
			name: {
				type: String,
				required: [true, 'Product Name is Required']
			},
			price: {
				type: Number,
				required: [true, 'Price is Required']
			},
		}],
		totalAmount: {
			type: Number,
			required: [true, 'totalAmount is Required']
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}		
	});

//Model
	module.exports = mongoose.model('History', historySchema);


