//Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors');
	const bodyParser = require("body-parser");
	const userRoutes = require('./routes/users'); 
	const productRoutes = require('./routes/products');
	const orderRoutes = require('./routes/orders');
	const storesRoutes = require('./routes/stores');
	const historyRoutes = require('./routes/history');

//Environment Setup
	dotenv.config(); 
	let account = process.env.CREDENTIALS;
	const port = process.env.PORT;

//Server Setup
	const app = express();
	app.use(express.json()); 
	app.use(cors());

//Database Connection
	mongoose.connect(account, { useNewUrlParser: true, useUnifiedTopology: true });
	const connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log('Database Connected'));

//Backend Routes
	app.use('/users', userRoutes); 
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);
	app.use('/stores', storesRoutes);
	app.use('/history', historyRoutes);

//For image upload
	app.use(express.static('public'));
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());

//Server Gateway Response
	app.get('/', (req, res) => {
		res.send("Welcome to HappiTum. Order your favorite food now. Have a Tasty Day!")
	})

	app.listen(port, () => {
		console.log(`API is Hosted on port ${port}`)
	})