//Dependencies and Modules
	const express = require('express');
	const multer = require('multer');
	const StoreController = require('../controllers/stores');
	const auth = require('../auth');

//Routing Component
	const route = express.Router();

//Destructure auth
	const {verify, verifyAdmin} = auth;

//1. Create Store (Admin Only)

	const storage = multer.diskStorage({
	  destination: function(req, file, cb) {
	    cb(null, './uploads/');
	  },
	  filename: function(req, file, cb) {
	    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
	  }
	});

	const upload = multer({ 
		storage: storage,
		limits: {
    		fileSize: 1024 * 1024 
  		},
	});

	route.post('/register', verify, verifyAdmin, upload.single('storeImage'), (req, res) => {
		StoreController.registerStore(req).then(result => {
			res.send(result);
		})
	})

//2. Retrieve All Stores
	route.get('/', (req, res) => {
		StoreController.getAllStores().then(result => res.send(result));
	})

//3. Retrieve Store's Products
	route.get('/:storeId/products', (req, res) => {
		StoreController.getStoreProducts(req.params.storeId).then(result => res.send(result));
	})

//4. Retrieve Store's Details
	route.get('/:storeId/details', (req, res) => {
		StoreController.getStoreDetails(req.params.storeId).then(result => res.send(result));
	})

//5. Update Store Information (Admin only)
	route.put('/:storeId', verify, verifyAdmin, (req, res) => {
		StoreController.updateStore(req.params.storeId, req).then(result => res.send(result));
	})

//6. Update Store Information with Image (Admin only)
	route.put('/:storeId/withImage', verify, verifyAdmin, upload.single('storeImage'), (req, res) => {
		StoreController.updateStoreImage(req.params.storeId, req).then(result => res.send(result));
	})


//7. Deactivate Store
	route.put('/:storeId/deactivate', verify, verifyAdmin, (req, res) => {
		StoreController.deactivateStore(req.params.storeId).then(result => res.send(result));
	})

//8. Activate Store
	route.put('/:storeId/activate', verify, verifyAdmin, (req, res) => {
		StoreController.activateStore(req.params.storeId).then(result => res.send(result));
	})


//9. Delete Store

	route.delete('/delete', verify, verifyAdmin, (req, res) => {
		StoreController.deleteStore(req).then(result => res.send(result));
	})


//Expose Route System
	module.exports = route;


	