//Dependencies and Modules
	const express = require('express');
	const OrderController = require('../controllers/orders');
	const auth = require('../auth');

//Routing Component
	const route = express.Router();

//Destructure auth
	const {verify, verifyAdmin, verifyNonAdmin} = auth;

//1. Create Order (Non-admin)
	route.post('/addOrder', verify, verifyNonAdmin, OrderController.addOrder);

//2. Retrieve all orders (Admin Only)
	route.get('/allOrders', verify, verifyAdmin, (req, res) => {
		OrderController.getAllOrders().then(result => res.send(result));
	});

//3. Edit Order
	route.put('/editOrder', verify, (req, res) => {
		OrderController.editOrder(req).then(result => res.send(result));
	})

//4. Remove Order
	route.delete('/remove', verify, (req, res) => {
		OrderController.removeOrder(req).then(result => res.send(result));
	})

//5. Remove Order Item
	route.delete('/deleteItem', verify, (req, res) => {
		OrderController.removeItem(req).then(result => res.send(result));
	})
	
//Expose Route System
	module.exports = route;


	