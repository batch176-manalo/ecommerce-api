//Dependencies and Modules
	const express = require('express'); 
	const UserController = require('../controllers/users'); 
	const auth = require('../auth');
	const User = require('../models/User'); 

//Routing Component
	const route = express.Router();

//Destructure auth
	const {verify, verifyAdmin} = auth;

//1. User Registration
	route.post('/register', (req, res) => {
		UserController.register(req.body).then(result => {
			res.send(result);
		})
	})
	
//2. User Authentication
	route.post('/login', (req, res) => {
		UserController.loginUser(req.body).then(result => res.send(result));
	})

//3. Set User as Admin (Admin Only)
	route.put('/:userId', verify, verifyAdmin, (req, res) => {

		let userIsAdmin = User.findById(req.params.userId).exec().then(user => {
			
			if (user.isAdmin === true) {
				res.send('User is already an Admin.');
			} else {
				UserController.setAsAdmin(req.params.userId).then(result => res.send(result));
			}		
		});	
	})


//4. Retrieve Authenticated User's Order
	route.get('/getOrders', verify, UserController.getUserOrders);

//5. Retrieve All Users (Admin Only)
	route.get('/getAllUsers', verify, verifyAdmin, (req, res) => {
		UserController.getAllUsers().then(result => res.send(result));
	})

//6. Retrieve User's Details
	route.get('/details', verify, (req, res) => {
		UserController.getProfile(req.user.id).then(result => res.send(result));
	})



//Expose Route System
	module.exports = route;

