//Dependencies and Modules
	const express = require('express');
	const multer = require('multer');
	const ProductController = require('../controllers/products');
	const auth = require('../auth');

//Routing Component
	const route = express.Router();

//Destructure auth
	const {verify, verifyAdmin} = auth;

//1. Create Product (Admin only) 

	const storage = multer.diskStorage({
	  destination: function(req, file, cb) {
	    cb(null, './uploads/');
	  },
	  filename: function(req, file, cb) {
	    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
	  }
	});

	const upload = multer({ 
		storage: storage,
		limits: {
    		fileSize: 1024 * 1024 
  		},
	});

	route.post('/create', verify, verifyAdmin, upload.single('productImage'), (req, res) => {
		ProductController.addProduct(req).then(result => res.send(result));
	})

//2. Retrieve all Active Products
	route.get('/active', (req, res) => {
		ProductController.getAllActive().then(result => res.send(result));
	})

//3. Retrieve all Inactive Products (Admin Only) - Additional Feature
	route.get('/inactive', verify, verifyAdmin, (req, res) => {
		ProductController.getAllinActive().then(result => res.send(result));
	})

//4. Retrieve Single Product
	route.get('/:productId', (req, res) => {
		ProductController.getProduct(req.params.productId).then(result => res.send(result));
	})

//5. Update Product Information (Admin only)
	route.put('/:productId', verify, verifyAdmin, (req, res) => {
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	})

//6. Update Product Information with Image (Admin only)
	route.put('/:productId/withImage', verify, verifyAdmin, upload.single('productImage'), (req, res) => {
		ProductController.updateProductImage(req.params.productId, req).then(result => res.send(result));
	})

//7. Archive Product (Admin only)
	route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	})

//8. Reactivate Product (Admin only) - Additional Feature
	route.put('/:productId/reactivate', verify, verifyAdmin, (req, res) => {
		ProductController.reactivateProduct(req.params.productId).then(result => res.send(result));
	})

//9. Remove Product (Admin only)

	route.delete('/delete', verify, verifyAdmin, (req, res) => {
		ProductController.deleteProduct(req).then(result => res.send(result));
	})

//Expose Route System
	module.exports = route;