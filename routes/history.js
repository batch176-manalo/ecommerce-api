//Dependencies and Modules
	const express = require('express');
	const auth = require('../auth');
	const HistoryController = require('../controllers/history');

//Routing Component
	const route = express.Router();

//Destructure auth
	const {verify, verifyNonAdmin } = auth;

//1. Move Orders to History Collection
	route.post('/checkout', verify, (req, res) => {
		HistoryController.moveToHistory(req).then(result => res.send(result));
	})

//2. View History
	route.get('/all', verify, (req, res) => {
		HistoryController.viewHistory(req).then(result => res.send(result));
	});

//Expose Route System
	module.exports = route;
