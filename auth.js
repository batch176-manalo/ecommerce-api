//Dependencies and Modules
	const jwt = require('jsonwebtoken');
	const dotenv = require('dotenv');

//Environment Variables Setup
	dotenv.config();
	const secret = process.env.SECRET;

//Token Creation
	module.exports.createAccessToken = (user) => {

		const data = {
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin
		};

		return jwt.sign(data, secret, {});

	}


//Token Verification
	module.exports.verify = (req, res, next) => {

		let token = req.headers.authorization;

		if(typeof token === 'undefined') {
			return res.send({auth: 'Failed. No token'});
		} else {

			token = token.slice(7, token.length);

			jwt.verify(token, secret, function(error, decodedToken) {

				if(error) {
					return res.send({
						auth: 'Failed',
						message: error.message
					})
				} else {
					req.user = decodedToken;
					next();
				}
			})
		}
	}

//Admin Verification
	module.exports.verifyAdmin = (req, res, next) => {
		if(req.user.isAdmin) {
			next();
		}else {
			return res.send({
				auth: "Failed",
				message: "Action Forbidden"
			})
		}
	}

//Non-Admin Verification
	module.exports.verifyNonAdmin = (req, res, next) => {
		if(req.user.isAdmin) {
			
			return res.send({
				auth: "Failed",
				message: "Admin"
			})

		} else {
			next();
		}
	}

